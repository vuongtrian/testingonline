import { Question } from './question.js'

export class FillInBlank extends Question {
  constructor(type, id, content, answers) {
    super(type, id, content, answers);
  }
  render(index) {
    return `
        <div>
            <p>Câu ${index}: ${this.content}</p>
            <input type = "text" id="answer-${this._id}"/>
        </div>
    `;
  }
  checkExact(){
    // 1.DOM tới input để lấy value
    let value = document.getElementById(`answer-${this._id}`).value;

    // chuyển về chữ thường để so sánh cho dễ
    value = value.toLowerCase();
    
    // 2.check value === answers[0].content => true
    const answer = this.answers[0].content.toLowerCase();

    if (value === answer) return true;
    return false;
  }
}

const newQuestion = new FillInBlank(1, 2, "Hôm nay là thứ mấy ?", [
    { content: "Thứ 2" },
    { content: "Thứ 4" },
    { content: "Thứ 6" },
    { content: "Thứ 7" },
  ]);
  
  console.log(newQuestion.render());