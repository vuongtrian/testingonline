import { Question } from './question.js'

export class MultipleChoice extends Question {
  constructor(type, id, content, answers) {
    super(type, id, content, answers);
  }
  render(index) {
    // In ra giao diện 4 đáp án
    var answersHTMLContent = "";
    // for (let i = 0; i < this.answers.length; i++) {
    for (let answ of this.answers) {
      answersHTMLContent += `
            <div>
                <input value="${answ._id}" class ="answer-${this._id}" type="radio" name="answer-${this._id}"/>
                <label>${answ.content}</label>
            </div>
        `;
    }

    //return về giao diện HTML của câu hỏi
    return `
        <div>
            <p>Câu ${index}: ${this.content}</p>
            ${answersHTMLContent}
        </div>
      `;
  }
  checkExact() {
    //1.Kiểm tra người dùng chọn ô nào => id câu trả lời
    // 1.1 DOM tới 4 input, kiểm tra thuộc tính checked === true => người dùng chọn ô nào => _id câu trả lời
    const inputList = document.getElementsByClassName(`answer-${this._id}`);

    let answerId;

    for (let item of inputList) {
      if (item.checked) {
        answerId = item.value;
        break;
      }
    }

    if (!answerId) return false;

    //2.Kiểm tra thuộc tính exact => kết quả (true /false)
    // 2.1 => tìm ra đối tượng đáp án trong mảng đáp án => exact
    for (let answer of this.answers){
      if (answer._id === answerId){
        return answer.exact;
      }
    }

    return false;
  }
}

// const newQuestion = new MultipleChoice(1, 2, "Hôm nay là thứ mấy ?", [
//   { content: "Thứ 2" },
//   { content: "Thứ 4" },
//   { content: "Thứ 6" },
//   { content: "Thứ 7" },
// ]);

// console.log(newQuestion.render());
