//1.let, var, const

//let giống var nhưng let không cho trùng tên biến trong cùng 1 khu vực
/**
 * let num = 1;
 * let num = 2;
 * => sẽ báo lỗi
 */

//phạm vụ sử dụng của let nhỏ hơn var  
/**
 * {
 *  let num = 1;
 * var num1 = 2;
 * }
 * => ra khỏi {} thì let không sử dụng lại đc
 */

//let không sử dụng cho hoisting


// 2.for

//ES6 cho phép 2 vòng for
/**
 * var chars = ["a", "b", "c", "d"];
 * 
 * for of:
 * for (let item of chars){ //item là tên biến
 * 
 * } 
 * 
 * for in:
 * for(let i in chars){
 * 
 * }
 */ 

 //3.Arrow function
 /**
  * THÔNG THƯỜNG
  * const demo = function (a, b){
  *     return (a+b);
  * }
  * 
  * ARROW FUNCTION
  * const demo = (a + b) => a + b;
  * 
  * const demo2 = () => ({name: "hieu"});
  * 
  * function thường đổi ngữ cảnh của this. Arrow function thì KHÔNG làm đổi ngữ cảnh của this
  */

//4.Default parameters
  /**
   * const calcSum = (a = 2 , b = 5) => {
   *    console.log (a + b);
   * }
   * 
   * calcSum (1) => kết quả là 1 + 5 = 6
   * calcSum () => kết quả là 2 + 5 = 7
   * 
   * Đây là tính năng nếu không truyền tham số, JS sẽ lấy tham số mặc định
   */

//5.Spread operator
/**
 * 5.1/Dùng để Copy Object
 * let student1 = {name: "Hieu", age: 12}
 * 
 * clone object - es5
 * let student2 = Object.assign({}, student1);
 * 
 * clone object - es6
 * let student2 = {...student1};
 * 
 * clone object là student2 sẽ copy toàn bộ thuộc tính của student1
 * 
 * let student2 = {...student1, email: 'dth@gmail.com', age: 15}
 *  -thuộc tính email đc thêm vào
 *  -age được update lại thành 15 vì đứng sau
 * 
 * 5.2/Dùng để copy array
 * let arr1 = [1,2,3,4,5];
 * 
 * let arr2 = [...arr1]
 *  -copy arr1 qua arr2
 * 
 * let arr3 = [0,...arr1,6]
 *  -mảng arr3 lúc này là [0,1,2,3,4,5,6]
 * 
 * 5.3/Nối array và nối Object
 * let arr1 = [1,2,3]
 * let arr2 = [4,5,6]
 * 
 * let arr3 = [...arr1, ...arr2]
 *  -Mảng arr3 lúc này là [1,2,3,4,5,6]
 * 
 * let student1 = {name: "hieu"};
 * let student2 = {age: 12};
 * 
 * let student = {...student1, ...student2}
 *  -student lúc này là {name: "hieu", age: 12}
 */

 //6.Rest parameter
 /**
  * const calcSum = (...nums) => {
  *     let sum = 0;
  *     for (let value of nums){
  *         sum += value;
  *     }
  * }
  * 
  * calcSum (1,2,3,4,5)
  * => sum = 15
  * Chức năng này cho phép người dùng truyền vào bao nhiêu tham số cũng đc
  */

//7. Object Literal (object chân phương)
/**
 * let keyName = "name";
 * let keyAge = "age";
 * let student = {[keyName]: "hiếu", [keyAge]: 12};
 * student lúc này là {name: "hiếu", age: 12}
 * Lấy giá trị của biến keyName là name
 */

 //8. String template
 /**
  * là dấu ``
  */

  //9.Destructuring (bóc tách phần tử)
  /**
   * let student = {name: "Hiếu", age: 12, email: "dth@gmail.com"};
   * 
   * const {name, age} = student;
   * console.log (name, age, student.email);
   * 
   */

  //10.High order function
  /**
   * 10.1/MAP
   * ---Cách cũ---
   * let nums = [1,2,3,4];
   * let newNum = [];
   * 
   * for (let num of nums){
   *    newNum.push(num + 1)
   * }
   * newNum = [2,3,4,5];
   * 
   * ---Cách mới / MAP---
   * let nums = [1,2,3,4];
   * let newNUM = nums.map((item) => item + 1)
   * 
   * 10.2/Filter
   * let nums = [1,2,3,4,5,6,7,8,9];
   * let Odd = nums.filter ((num) => num % 2 === 1);
   * Ta được mảng Odd = [1,3,5,7,9]
   */