/**
 * Dự án: web trắc nghiệm
 * Chức năng:
 *  1.Lấy danh sách câu hỏi từ DataBase
 *  2.Hiện danh sách câu hỏi ra màn hình
 *  3.Chấm điểm
 */

import {MultipleChoice} from '../Models/multipleChoice.js'
import {FillInBlank} from '../Models/fillInBlank.js'

//function 1: Lấy danh sách câu hỏi từ DataBase
let questionList = [];

const fetchQuestion = () => {
  axios({
    url: "./playground/DeThiTracNghiem.json",
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      // questionList = res.data;
      mapData(res.data);
      //render câu hỏi ra màn hình
      renderQuestion();
    })
    .catch(function (err) {
      console.log(err);
    });
};

// fuction 2: Hiển thị câu hỏi ra màn hình
const renderQuestion = () => {
  let htmlContent = "";
  for (let i in questionList) {
    htmlContent += questionList[i].render(+i + 1);
  }
  document.getElementById("questionContainer").innerHTML = htmlContent;
};

//function 3: map từ ds câu hỏi của backend ra thành danh sách đối tượng câu hỏi của mình
/**
 * @params {Array} data - data from backend
 * @return {Array} questionList - my data
 */
// const mapData = (data) => {
//   for (let item of data) {
//     let myQuestionObject;
//     if (item.questionType == 1) {
//       myQuestionObject = new MultipleChoice(
//         item.questionType,
//         item._id,
//         item.content,
//         item.answers
//       );
//     } else {
//       myQuestionObject = new FillInBlank(
//         item.questionType,
//         item._id,
//         item.content,
//         item.answers
//       );
//     }
//     questionList.push(myQuestionObject);
//   }
// };

const mapData = (data = []) => {
  questionList = data.map((item) => {
    const { questionType, _id, content, answers } = item;

    if (item.questionType == 1) {
      return new MultipleChoice(questionType, _id, content, answers);
    }
    return new FillInBlank(questionType, _id, content, answers);
  });
};

// function 4: tính điểm, hiện kết quả
const submit = () => {
  let result = 0;

  for (let question of questionList) {
    if (question.checkExact()) result++;
  }
  console.log("Kết quả:", result + " / " + questionList.length);
};

document.getElementById("btnSubmit").addEventListener("click", submit);

fetchQuestion();
